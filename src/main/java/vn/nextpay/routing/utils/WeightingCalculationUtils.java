/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.nextpay.routing.utils;

/**
 * @author hieunm
 */
public class WeightingCalculationUtils {
    public static Double getWeighResult(Double inputVal, Double high, Double low, Double sHigh, Double sLow, Double weighPoint) {
        Double inp = 0D;
        Double scale;
        Double weigh;//o	Align inp by
        if (high > low) {
            inp = Math.max(Math.min(high, inputVal), low);
        } else if (high < low) {
            inp = Math.min(Math.max(high, inputVal), low);
        }
        System.out.println("inp ==>" + inp);
        //o	Calculate: scale
        scale = Math.abs((inp - low) / (high - low) * (sHigh - sLow) + sLow);
        System.out.println("scale ==>" + scale);
        //o	Calculate: weightcriterion
        weigh = (scale * weighPoint);
        return weigh;
    }

    public static Double getWeighResult(Integer inputVal, Double high, Double low, Double sHigh, Double sLow, Double weighPoint) {
        Double inp = 0D;
        Double scale;
        Double weigh;//o	Align inp by
        if (high > low) {
            inp = Math.max(Math.min(high, inputVal), low);
        } else if (high < low) {
            inp = Math.min(Math.max(high, inputVal), low);
        }
        //o	Calculate: scale
        scale = Math.abs((inp - low) / (high - low) * (sHigh - sLow) + sLow);
        //o	Calculate: weightcriterion
        weigh = (scale * weighPoint);
        return weigh;
    }

    public static boolean weighIsErr(Double high, Double low, Double sHigh, Double sLow, Double weighPoint) {
        if (high == null || low == null || sHigh == null || sLow == null || weighPoint == null) {
            return true;
        }
        return high == low;
    }

    public static void main(String[] args) {
        Double a = 0D;
        a = getWeighResult(0.2D, 0.03D, 0.0001D, 0.0D, 100D, 0.5D);
        System.out.println("ket qua" + a.intValue());

    }
}
