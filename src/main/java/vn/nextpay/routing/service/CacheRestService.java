package vn.nextpay.routing.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.entity.ConfigSystem;
import vn.nextpay.routing.dao.entity.MobileUserInfo;
import vn.nextpay.routing.dao.entity.WeightingCache;
import vn.nextpay.routing.dao.entity.sub_document.PaymentClass;
import vn.nextpay.routing.dao.enumeration.EnumConfigSystemKey;

import java.time.ZonedDateTime;
import java.util.List;

@Service
@Slf4j
public class CacheRestService extends BaseService {
    public void insertNewCache(PaymentClass paymentClass, MobileUserInfo mobileUserInfo) {
        WeightingCache weightingCache = new WeightingCache();
        weightingCache.setInCardType(paymentClass.getCardType());
        weightingCache.setInAcqBank(paymentClass.getAcqBank());
        weightingCache.setInMcc(paymentClass.getMcc());
        weightingCache.setInMid(paymentClass.getLstMid());
        Long day = 10L;
        ConfigSystem configSystem = configSystemService.findByKey(EnumConfigSystemKey.EXPIRE_DATE_CACHE);
        if (configSystem != null) {
            day = Long.parseLong(configSystem.getValue());
        }
        weightingCache.setExpiredDate(ZonedDateTime.now().plusDays(day));
        weightingCache.setOutAcqCodeResult(mobileUserInfo.getAcquirer());
        weightingCacheService.save(weightingCache);
    }

    public void cacheDeleteAll() {
        weightingCacheService.deleteAll();
    }

    public List<WeightingCache> cacheRetrieveAll() {
        return weightingCacheService.findAllActiveCache();
    }
}
