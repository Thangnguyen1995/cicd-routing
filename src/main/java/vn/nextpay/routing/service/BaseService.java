package vn.nextpay.routing.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.service.AuditTrailService;
import vn.nextpay.routing.dao.service.CardListService;
import vn.nextpay.routing.dao.service.ConfigSystemService;
import vn.nextpay.routing.dao.service.DefaultHostService;
import vn.nextpay.routing.dao.service.ExcludedHostService;
import vn.nextpay.routing.dao.service.HostInfoService;
import vn.nextpay.routing.dao.service.LocalBinService;
import vn.nextpay.routing.dao.service.MerchantInfoService;
import vn.nextpay.routing.dao.service.MobileUserInfoService;
import vn.nextpay.routing.dao.service.WeightingCacheService;

@Service
public class BaseService {
    @Autowired
    protected AuditTrailService auditTrailService;
    @Autowired
    protected ConfigSystemService configSystemService;
    @Autowired
    protected WeightingCacheService weightingCacheService;
    @Autowired
    protected MerchantInfoService merchantInfoService;
    @Autowired
    protected LocalBinService localBinService;
    @Autowired
    protected CardListService cardListService;
    @Autowired
    protected ExcludedHostService excludedHostService;
    @Autowired
    protected HostInfoService hostInfoService;
    @Autowired
    protected MobileUserInfoService mobileUserInfoService;
    @Autowired
    protected DefaultHostService defaultHostService;
}
