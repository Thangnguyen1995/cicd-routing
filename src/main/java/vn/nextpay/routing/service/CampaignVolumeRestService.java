package vn.nextpay.routing.service;

import org.springframework.stereotype.Service;

@Service
public class CampaignVolumeRestService extends BaseService {

    public void listAllCampaignColumn() {

    }

    public void saveCampaignColumn() {

    }

    public void updateCampaignColumn() {

    }

    public void deleteCampaignColumn() {

    }

    public void updatePerformanceCampaignColumn() {

    }

}
