package vn.nextpay.routing.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.multiacq.common.enums.BasicStatusCode;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.multiacq.common.error.BusinessLogicError;
import vn.nextpay.multiacq.common.utils.ComplementUtils;
import vn.nextpay.multiacq.common.utils.DateTimeUtil;
import vn.nextpay.routing.GetAcqFromCoreWfRes;
import vn.nextpay.routing.SuspendAcquirerReq;
import vn.nextpay.routing.SuspendAcquirerRes;
import vn.nextpay.routing.dao.entity.DefaultHost;
import vn.nextpay.routing.dao.entity.DefaultPair;
import vn.nextpay.routing.dao.entity.ExcludedHost;
import vn.nextpay.routing.dao.entity.HostInfo;
import vn.nextpay.routing.dao.entity.LocalBin;
import vn.nextpay.routing.dao.entity.MerchantInfo;
import vn.nextpay.routing.dao.entity.MobileUserInfo;
import vn.nextpay.routing.dao.entity.WeightingCache;
import vn.nextpay.routing.dao.entity.sub_document.PaymentClass;
import vn.nextpay.routing.dao.entity.sub_document.PaymentData;
import vn.nextpay.routing.dao.enumeration.WeighCriterion;
import vn.nextpay.routing.dao.service.DefaultPairService;
import vn.nextpay.routing.dao.service.WeightingCalculation;
import vn.nextpay.routing.dao.service.WeightingParamService;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AcquirerRestService extends BaseService {
    @Autowired
    private CacheRestService cacheRestService;

    @Autowired
    private DefaultPairService defaultPairService;

    @Autowired
    private WeightingParamService weightingParamService;

    public SuspendAcquirerRes suspendAnAcq(SuspendAcquirerReq request) {
        Optional<HostInfo> hostInfoOptional = hostInfoService.findByCode(request.getAcqCode());
        if (!hostInfoOptional.isPresent()) {
            log.error("acquirer code not found");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND,
                    "acquirer code not found");
        }
        ExcludedHost excludedHost = new ExcludedHost();
        ComplementUtils.complement(hostInfoOptional.get(), excludedHost);
        excludedHost.setReason(request.getReason());

        excludedHost.setTimeFrom(DateTimeUtil.timeProtoToZoneVN(request.getTimeFrom()));
        excludedHost.setTimeEnd(DateTimeUtil.timeProtoToZoneVN(request.getTimeEnd()));
        excludedHostService.save(excludedHost);
        return SuspendAcquirerRes.newBuilder().build();
    }

    public SuspendAcquirerRes removeAnAcq(SuspendAcquirerReq request) {
        Optional<HostInfo> hostInfoOptional = hostInfoService.findByCode(request.getAcqCode());
        if (!hostInfoOptional.isPresent()) {
            log.error("acquirer code not found");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND,
                    "acquirer code not found");
        }
        hostInfoService.deleteByCode(request.getAcqCode());
        return SuspendAcquirerRes.newBuilder().build();
    }

    public void updateAccquirer(HostInfo inputHostInfo) {
        Optional<HostInfo> hostInfoOptional = hostInfoService.findByCode(inputHostInfo.getCode());
        if (hostInfoOptional.isPresent()) {
            HostInfo hostInfo = hostInfoOptional.get();
            ComplementUtils.complement(inputHostInfo, hostInfo);
            hostInfoService.save(hostInfo);
        }
    }

    public List<HostInfo> acqRetrieveAll() {
        return hostInfoService.findAll();
    }

    public GetAcqFromCoreWfRes getAcqForPaymentData(PaymentData paymentData) {
        String muid = paymentData.getMobileUser();
        String pan = paymentData.getNumberPAN();
        if (StringUtils.isBlank(muid) || StringUtils.isBlank(pan)) {
            log.error("Param invalid");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "Param invalid");
        }

        //tim thong tin tid,mid, acquirer theo mobile userid truyen vao
        List<MobileUserInfo> mobileUserInfoInput = mobileUserInfoService.findByMobileUserId(muid);
        if (mobileUserInfoInput == null || mobileUserInfoInput.size() < 1) {
            //Co loi tra ve
            log.error("MobileUserId not found");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "MobileUserId not found");
        }
        List<WeighCriterion> weighCriterionList = weightingParamService.weighCriterionList();
        if (weighCriterionList == null || weighCriterionList.size() < 1) {
            //Co loi tra ve
            log.error("weighCriterionList not found");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "weighCriterionList not found");
        }

        //step1
        log.info("Routing step 1 executing... ");
        List<MobileUserInfo> mobileUserInfoList = step1(mobileUserInfoInput);
        if (mobileUserInfoList == null || mobileUserInfoList.isEmpty()) {
            log.error("step 1 is empty");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "step 1 is empty");
        } else if (mobileUserInfoList.size() == 1) {
            return buildAcquirerSuccess(mobileUserInfoList.get(0));
        }

        //step2a
        log.info("Routing step 2a executing... ");
        MobileUserInfo mobileUserInfoStep2a = step2a(mobileUserInfoList);
        if (mobileUserInfoStep2a != null) {
            return buildAcquirerSuccess(mobileUserInfoStep2a);
        }

        PaymentClass paymentClass = getPaymentClass(mobileUserInfoList, pan);
        //step2b
        log.info("Routing step 2b executing... ");
        MobileUserInfo mobileUserInfoStep2b = step2b(mobileUserInfoList, paymentClass);
        if (mobileUserInfoStep2b != null) {
            return buildAcquirerSuccess(mobileUserInfoStep2b);
        }
        //step3
        log.info("Routing step 3 executing... ");
        MobileUserInfo mobileUserInfoStep3 = step3(mobileUserInfoList, paymentClass, weighCriterionList);
        if (mobileUserInfoStep3 != null) {
            return buildAcquirerSuccess(mobileUserInfoStep3);
        }
        log.error("System not found");
        throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "System not found");
    }

    /***
     * Find All ACQ from mobileUserId
     * and remove acq from exclude host
     */
    private List<MobileUserInfo> step1(List<MobileUserInfo> mobileUserInfoList) {
        List<ExcludedHost> excludedHostList = excludedHostService.findInCurrent(ZonedDateTime.now());// can xu ly cache cho thang nay
        mobileUserInfoList.removeIf(item -> item.getStatus().equals(CommonStatus.SUSPENDED));
        if (mobileUserInfoList == null || mobileUserInfoList.size() < 1) {
            log.error("mobileUserInfo is empty");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "mobileUserInfo is empty");
        }
        for (ExcludedHost excludedHost : excludedHostList) {
            String accquirer = excludedHost.getCode();
            mobileUserInfoList.removeIf(item -> item.getAcquirer().equals(accquirer));
        }
        if (mobileUserInfoList == null || mobileUserInfoList.size() < 1) {
            log.error("mobileUserInfo is empty");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "mobileUserInfo is empty");
        }
        return mobileUserInfoList;
    }

    /**
     * Get ACQ default from list ACQ step1
     */
    private MobileUserInfo step2a(List<MobileUserInfo> mobileUserInfoList) {
        List<DefaultHost> defaultHosts = defaultHostService.findInCurrent(ZonedDateTime.now());
        for (DefaultHost defaultHost : defaultHosts) {
            for (MobileUserInfo mobileUserInfo : mobileUserInfoList) {
                if (defaultHost.getCode().equals(mobileUserInfo.getAcquirer())) {
                    return mobileUserInfo;
                }
            }
        }
        return null;
    }

    /**
     * Get Acq default with PaymentClass
     */
    private MobileUserInfo step2b(List<MobileUserInfo> mobileUserInfoList, PaymentClass paymentClass) {
        //generate PaymentClass
        Optional<DefaultPair> defaultPairOptional = defaultPairService.findByPaymentClass(paymentClass);
        if (defaultPairOptional.isPresent()) {
            DefaultPair defaultPair = defaultPairOptional.get();
            for (MobileUserInfo mobileUserInfo : mobileUserInfoList) {
                if (mobileUserInfo.getAcquirer().equalsIgnoreCase(defaultPair.getAcqResultCode())) {
                    return mobileUserInfo;
                }
            }
        }
        return null;
    }

    /**
     * Caculate weighting with payment class
     */
    private MobileUserInfo step3(List<MobileUserInfo> mobileUserInfoList, PaymentClass paymentClass, List<WeighCriterion> weighCriterionList) {
        List<String> lstAcquirer = mobileUserInfoList.stream().map(item -> item.getAcquirer()).collect(Collectors.toList());
        //checking weight cache
        WeightingCache weightingCache = weightingCacheService.findActiveWeightingByPaymentClass(paymentClass);
        if (weightingCache != null) {
            log.info("Cache info: {}", weightingCache);
            return buildAcquirerStep3(weightingCache.getOutAcqCodeResult(), mobileUserInfoList, paymentClass, true);
        }

        String acquirer = calculateRoutingForPaymentClass(paymentClass, lstAcquirer, weighCriterionList);
        return buildAcquirerStep3(acquirer, mobileUserInfoList, paymentClass, false);
    }


    private MobileUserInfo buildAcquirerStep3(String acquirer, List<MobileUserInfo> mobileUserInfoList,
                                              PaymentClass paymentClass, boolean isCached) {
        MobileUserInfo result = null;
        if (!StringUtils.isBlank(acquirer)) {
            Optional<MobileUserInfo> matchingObject = mobileUserInfoList.stream().
                    filter(p -> p.getAcquirer().equals(acquirer)).
                    findFirst();
            result = matchingObject.get();
        }
        if (result != null && !isCached) {
            cacheRestService.insertNewCache(paymentClass, result);
        }
        return result;
    }


    private PaymentClass getPaymentClass(List<MobileUserInfo> mobileUserInfoList, String pan) {
        //step 0: get data from PaymentClass
        String cardType;
        String issueBank;
        String mcc;
        String merchantFk = mobileUserInfoList.get(0).getMerchantFk();
        String mposMid = mobileUserInfoList.get(0).getMposMid();
        String mposTid = mobileUserInfoList.get(0).getMposTid();
        List<String> lstMid = mobileUserInfoList.stream().map(item -> item.getMid()).sorted().collect(Collectors.toList());

        Optional<MerchantInfo> merchantInfo = merchantInfoService.findByMerchantFk(merchantFk);
        if (!merchantInfo.isPresent()) {
            return null;
        }
        mcc = merchantInfo.get().getMcc();


        if (pan.length() < 10) {
            return null;
        }
        String pan6 = pan.substring(0, 6);
        String pan4 = pan.substring(pan.length() - 4);
        if (!NumberUtils.isNumber(pan6.trim())) {
            return null;
        }
        if (!NumberUtils.isNumber(pan4.trim())) {
            return null;
        }
        String first10Digit = pan6 + pan4;
        cardType = cardListService.getCardType(first10Digit);
        issueBank = "OTHER";
        List<LocalBin> visaLocalBins = localBinService.findAllActive();
        if (visaLocalBins != null && visaLocalBins.size() >= 1) {
            for (LocalBin visaLocalBin : visaLocalBins) {
                if (visaLocalBin.getBin().equals(pan6)) {
                    if ("VISA".equals(cardType) || "MASTER".equals(cardType) || "JCB".equals(cardType) || "LOCAL".equals(cardType)) {
                        cardType = cardType + "_LOCAL";
                    }
                    issueBank = visaLocalBin.getBank();
                    break;
                }
            }
        }
        PaymentClass paymentClass = new PaymentClass(cardType, mcc, issueBank, lstMid, mposMid, mposTid);
        log.info("paymentClass info: {}", paymentClass);
        return paymentClass;
    }

    private GetAcqFromCoreWfRes buildAcquirerSuccess(MobileUserInfo mobileUserInfo) {
        //Lay thong tin tu HostInfo
        GetAcqFromCoreWfRes response = null;
        Optional<HostInfo> hostInfoOptional = hostInfoService.findByCode(mobileUserInfo.getAcquirer());
        HostInfo hostInfo = null;
        if (!hostInfoOptional.isPresent()) {
            log.error("host info not found");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "host info not found");
        }
        hostInfo = hostInfoOptional.get();
        response = GetAcqFromCoreWfRes.newBuilder()
                .setId(hostInfo.getId())
                .setCode(hostInfo.getCode())
                .setCountry(hostInfo.getCountry())
                .setTid(mobileUserInfo.getTid())
                .setMid(mobileUserInfo.getMid())
                .build();
        return response;
    }

    public Double calculateWeightedValueForPaymentClass(PaymentClass paymentClass, String acquirer, List<WeighCriterion> lsWeighCriterion) {
        Double sumWeighVal = 0D;
        for (WeighCriterion weighCriterion : lsWeighCriterion) {
            Double weighVal = calculateCriterionValueForPaymentClass(paymentClass, acquirer, weighCriterion);
            if (weighVal == null) {
                log.error("calculate criterion value for payment class error on criterion: {}", weighCriterion);
                throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND,
                        "calculate criterion value for payment class error on criterion: "
                                + weighCriterion.getName() + ", acquirer: " + acquirer);
            }
            log.info("===calculateWeightedValueForPaymentClass acquirer ==> {}, weighCriterion ===> {}, point ==> {}, paymentClass ===> {}", acquirer, weighCriterion, weighVal, paymentClass);
            sumWeighVal = sumWeighVal + weighVal;
        }
        return sumWeighVal;
    }

    public Double calculateCriterionValueForPaymentClass(PaymentClass paymentClass, String acquirer, WeighCriterion weighCriterion) {
        Double weighVal = null;
        WeightingCalculation weightingService = weightingParamService.getWeighCriterionService(weighCriterion);
        if (weightingService == null) {
            log.error("weighting criterion service: " + weighCriterion.getName() + " not found");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "weighting criterion service: " + weighCriterion.getName() + " not found");
        }
        weighVal = weightingService.getWeighValue(paymentClass, acquirer, weighCriterion);
        return weighVal;
    }

    public String calculateRoutingForPaymentClass(PaymentClass paymentClass, List<String> lsAcquirers, List<WeighCriterion> lsWeighCriterion) {
        String result = "";
        Map<String, Double> calculateMap = new HashMap<>();
        for (String acquirer : lsAcquirers) {
            Double weighValAcquirer = calculateWeightedValueForPaymentClass(paymentClass, acquirer, lsWeighCriterion);
            if (weighValAcquirer == null) {
                log.error("calculate weighted value for payment class error on paymentClass {}", paymentClass);
                throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND,
                        "calculate weighted value for payment class error on paymentClass: "
                                + paymentClass.getAcqBank() + "|"
                                + paymentClass.getCardType() + "|"
                                + paymentClass.getLstMid() + "|"
                                + ", acquirer: " + acquirer);
            }
            calculateMap.put(acquirer, weighValAcquirer);
        }
        log.info("calculateMap == > {}", calculateMap);
        double max = Collections.max(calculateMap.values());
        for (Map.Entry<String, Double> entry : calculateMap.entrySet()) {
            if (entry.getValue() == max) {
                result = entry.getKey();
                break;
            }
        }
        return result;
    }


}
