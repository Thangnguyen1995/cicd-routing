package vn.nextpay.routing.configuration;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfiguration {

    public static final String RETRIEVE_PAYMENT_CLASS_RTNG_DATA = "retrieve-payment-class-routing-data";
    public static final String RETRIEVE_EXCLUDE_HOST = "retrieve-exclude-host";
}
