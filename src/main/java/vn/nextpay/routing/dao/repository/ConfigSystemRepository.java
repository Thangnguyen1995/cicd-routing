package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.nextpay.routing.dao.entity.ConfigSystem;
import vn.nextpay.routing.dao.enumeration.EnumConfigSystemKey;

public interface ConfigSystemRepository extends MongoRepository<ConfigSystem, String> {
    ConfigSystem findByKey(EnumConfigSystemKey key);

}