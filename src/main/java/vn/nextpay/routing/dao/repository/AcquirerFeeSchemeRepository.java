package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import vn.nextpay.routing.dao.entity.AcquirerFeeScheme;

import java.util.List;

public interface AcquirerFeeSchemeRepository extends MongoRepository<AcquirerFeeScheme, String> {
    @Query("{ 'cardType': ?0}")
    List<AcquirerFeeScheme> findByCardType(String cardType);

    List<AcquirerFeeScheme> findByCardTypeLike(String cardType);

    @Query("{'acquirer': {$regex: ?0 }}")
    List<AcquirerFeeScheme> findByAcquirer(String acquirer);

    @Query("{$and: [{'acquirer': ?0}, {'status': 'ACTIVE'}, {$or: [{ 'cardType': ?1}, {'cardType': null}]}, {$or: [{ 'mid': {$in : ?2}}, {'mid': null}]}, {$or: [{ 'mcc': ?3}, {'mcc': null}]}]}")
    List<AcquirerFeeScheme> findByAcquirer(String acquirer, String cardType, List<String> lstMid, String mcc);
}
