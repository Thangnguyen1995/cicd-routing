package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.CardList;

import java.util.List;
import java.util.Optional;

public interface CardListRepository extends MongoRepository<CardList, String> {
    Optional<CardList> findById(String id);

    Optional<CardList> findByName(String name);

    List<CardList> findAllByStatus(CommonStatus status);
}