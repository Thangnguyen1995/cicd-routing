package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import vn.nextpay.routing.dao.entity.DefaultHost;

import java.time.ZonedDateTime;
import java.util.List;

public interface DefaultHostRepository extends MongoRepository<DefaultHost, String> {
    @Query("{$and: [{'timeFrom': { $lte: ?0}}, {'timeEnd' : { $gte: ?1}}, {status : 'ACTIVE'}]}")
    List<DefaultHost> findCurrentConfig(ZonedDateTime timeFrom, ZonedDateTime timeEnd);
}