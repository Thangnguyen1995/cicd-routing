package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import vn.nextpay.routing.dao.entity.AcquirerPreference;

import java.util.List;

public interface AcquirerPreferenceRepository extends MongoRepository<AcquirerPreference, String> {
    @Query("{ 'acquirer': ?0}")
    List<AcquirerPreference> findByAcquirer(String acquirer);

}
