package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.WeightingParam;

import java.util.List;

public interface WeightingParamRepository extends MongoRepository<WeightingParam, String> {
    WeightingParam findByCode(String code);

    List<WeightingParam> findByCodeLike(String code);

    @Query(value = "{ 'code': ?0}", delete = true)
    WeightingParam deleteByCode(String code);

    List<WeightingParam> findByStatus(CommonStatus status);

}
