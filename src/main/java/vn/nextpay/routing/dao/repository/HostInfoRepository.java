package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.nextpay.routing.dao.entity.HostInfo;

import java.util.Optional;

public interface HostInfoRepository extends MongoRepository<HostInfo, String> {
    Optional<HostInfo> findById(String id);

    Optional<HostInfo> findByCode(String code);

    HostInfo deleteByCode(String code);
}