package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.nextpay.routing.dao.entity.AuditTrail;

public interface AuditTrailRepository extends MongoRepository<AuditTrail, String> {
}