package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.LocalBin;

import java.util.List;
import java.util.Optional;

public interface LocalBinRepository extends MongoRepository<LocalBin, String> {
    Optional<LocalBin> findById(String id);

    LocalBin findByBin(String bin);

    List<LocalBin> findAllByStatus(CommonStatus status);
}