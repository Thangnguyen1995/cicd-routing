package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.nextpay.routing.dao.entity.MobileUserInfo;

import java.util.List;

public interface MobileUserInfoRepository extends MongoRepository<MobileUserInfo, String> {
    List<MobileUserInfo> findByMobileUserId(String muid);

    List<MobileUserInfo> findByAcquirer(String acquirer);
}