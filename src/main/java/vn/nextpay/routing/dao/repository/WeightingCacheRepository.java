package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import vn.nextpay.routing.dao.entity.WeightingCache;

import java.time.ZonedDateTime;
import java.util.List;

public interface WeightingCacheRepository extends MongoRepository<WeightingCache, String> {

    @Query("{$and: [{'inCardType': ?0}, {'inMcc': ?1}, {'inAcqBank' : ?2}, {'inMid': ?3}, {'expiredDate' : { $gte: ?4}}]}")
    List<WeightingCache> findActiveWeightingByPaymentClass(String inCardType, String inMcc,
                                                           String inAcqBank, List<String> inMid, ZonedDateTime now);

    @Query("{'expiredDate' : { $gte: ?0}}")
    List<WeightingCache> findAllActiveCache(ZonedDateTime now);


    @Query(value = "{'expiredDate' : { $lte: ?0}}", delete = true)
    void deleteAllInactiveCache(ZonedDateTime now);

}
