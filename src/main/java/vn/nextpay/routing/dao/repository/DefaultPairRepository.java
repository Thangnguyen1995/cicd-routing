package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import vn.nextpay.routing.dao.entity.DefaultPair;

import java.util.List;
import java.util.Optional;

public interface DefaultPairRepository extends MongoRepository<DefaultPair, String> {
    @Query("{$and: [{'cardType': ?0}, {'mcc': ?1}, {'acqBank': ?2}, { 'mid': {$in : ?3}}, { 'mposMid': ?4}, { 'mposTid': ?5}]}")
    Optional<DefaultPair> findByPaymentClass(String cardType, String mcc, String acqBank, List<String> mid, String mposMid, String mposTid);
}