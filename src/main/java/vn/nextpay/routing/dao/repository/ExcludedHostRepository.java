package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import vn.nextpay.routing.dao.entity.ExcludedHost;

import java.time.ZonedDateTime;
import java.util.List;

public interface ExcludedHostRepository extends MongoRepository<ExcludedHost, String> {
    @Query("{$and: [{'timeFrom': { $lte: ?0}}, {'timeEnd' : { $gte: ?1}}, {status : 'ACTIVE'}]}")
    List<ExcludedHost> findCurrentConfig(ZonedDateTime timeFrom, ZonedDateTime timeEnd);
}