package vn.nextpay.routing.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import vn.nextpay.routing.dao.entity.MerchantInfo;

import java.util.Optional;

public interface MerchantInfoRepository extends MongoRepository<MerchantInfo, String> {
    Optional<MerchantInfo> findById(String id);

    Optional<MerchantInfo> findByMerchantFk(String merchantFk);
}