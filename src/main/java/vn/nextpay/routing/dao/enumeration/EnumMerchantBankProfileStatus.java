package vn.nextpay.routing.dao.enumeration;

public enum EnumMerchantBankProfileStatus {
    ACTIVE, INACTIVE
}
