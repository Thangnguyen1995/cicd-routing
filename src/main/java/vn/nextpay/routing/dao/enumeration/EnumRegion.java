package vn.nextpay.routing.dao.enumeration;

public enum EnumRegion {
    Domestic, International
}
