package vn.nextpay.routing.dao.enumeration;

public enum WeighCriterion {
    ACQUIRER_FEE,
    ACQUIRER_PREFERENCE,
    UVOL_DAILY_PERF,
    LVOL_DAILY_PERF;

    public static WeighCriterion getWeighCriterionByName(String name) {
        for (WeighCriterion s : WeighCriterion.values()) {
            if (name.equalsIgnoreCase(s.getName())) {
                return s;
            }
        }
        return null;
    }

    public String getName() {
        return this.name();
    }
}
