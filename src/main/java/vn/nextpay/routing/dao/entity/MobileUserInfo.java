package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.nextpay.multiacq.common.enums.CommonStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Document(collection = "mobileUserInfo")
public class MobileUserInfo extends BasicTable {
    private String mobileUserId;
    private String tid;
    private String mid;
    private String acquirer;
    private String merchantFk;
    private String mposMid;
    private String mposTid;
    @Enumerated(EnumType.STRING)
    private CommonStatus status;

}
