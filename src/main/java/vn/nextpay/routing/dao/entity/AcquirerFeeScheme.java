package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.nextpay.multiacq.common.enums.CommonStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Document(collection = "acquirerFeeScheme")
public class AcquirerFeeScheme extends BasicTable {
    private String acquirer;
    private String cardType;
    private Double feeOnus;
    private Double feeOffus;
    private String mid;
    private String mcc;
    @Enumerated(EnumType.STRING)
    private CommonStatus status;

}
