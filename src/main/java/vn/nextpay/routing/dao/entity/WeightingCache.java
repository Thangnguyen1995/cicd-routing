package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@Document(collection = "weightingCache")
public class WeightingCache extends BasicTable {
    private String inCardType;
    private String inMcc;
    private String inAcqBank;
    private List<String> inMid;
    private String outAcqCodeResult;
    private ZonedDateTime createdDate;
    private ZonedDateTime expiredDate;
}
