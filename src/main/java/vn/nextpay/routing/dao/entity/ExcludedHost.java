package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.nextpay.multiacq.common.enums.CommonStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.ZonedDateTime;

@Data
@Document(collection = "excludedHost")
public class ExcludedHost extends BasicTable {
    private String code;
    private String reason;
    private ZonedDateTime timeFrom;
    private ZonedDateTime timeEnd;
    @Enumerated(EnumType.STRING)
    private CommonStatus status;
}
