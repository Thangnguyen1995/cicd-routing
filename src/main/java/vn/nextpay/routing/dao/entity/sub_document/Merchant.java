package vn.nextpay.routing.dao.entity.sub_document;

import lombok.Data;

@Data
public class Merchant {
    private String id;
    private String code;
    private String name;
    private String phoneNumber;
    private String passPort;
    private String address;
    private String email;
    private String country;

}
