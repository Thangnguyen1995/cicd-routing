package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "hostInfo")
public class HostInfo extends BasicTable {
    @Indexed(unique = true)
    private String code;
    private String country;
    private String paymentBaseUrl;
}
