package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.nextpay.multiacq.common.enums.CommonStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Document(collection = "weightingParam")
public class WeightingParam extends BasicTable {
    private String code;
    private String unit;
    private Double weightPoint;
    private Double low;
    private Double high;
    private Double scaleLow;
    private Double scaleHigh;
    @Enumerated(EnumType.STRING)
    private CommonStatus status;
}
