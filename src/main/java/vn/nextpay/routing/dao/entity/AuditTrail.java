package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "auditTrail")
public class AuditTrail extends BasicTable {
    private String dbName;
    private String action;
    private Object description;
}
