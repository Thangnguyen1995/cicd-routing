package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.nextpay.multiacq.common.enums.CommonStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Document(collection = "cardList")
public class CardList extends BasicTable {
    private String name;
    private Integer cardId;
    private String binRange;
    @Enumerated(EnumType.STRING)
    private CommonStatus status;
}
