package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.nextpay.multiacq.common.enums.CommonStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Document(collection = "localBin")
public class LocalBin extends BasicTable {
    private String bin;
    private String bank;
    @Enumerated(EnumType.STRING)
    private CommonStatus status;
}
