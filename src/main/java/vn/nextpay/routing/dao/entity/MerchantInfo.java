package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.nextpay.multiacq.common.enums.CommonStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Document(collection = "merchantInfo")
public class MerchantInfo extends BasicTable {
    private String merchantFk;
    private String authoriserEmail;
    private String mcc;
    @Enumerated(EnumType.STRING)
    private CommonStatus status;
}
