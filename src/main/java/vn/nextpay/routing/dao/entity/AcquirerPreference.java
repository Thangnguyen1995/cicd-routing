package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "acquirerPreference")
public class AcquirerPreference extends BasicTable {
    private String acquirer;
    private Integer preference;
    private String reason;
}
