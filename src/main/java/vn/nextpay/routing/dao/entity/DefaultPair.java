package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "defaultPair")
public class DefaultPair extends BasicTable {
    private String cardType;
    private String mcc;
    private String acqBank;
    private String acqResultCode;
    private String mid;
    private String mposMid;
    private String mposTid;
}
