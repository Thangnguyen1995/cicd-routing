package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import vn.nextpay.routing.dao.enumeration.EnumConfigSystemKey;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Document(collection = "configSystem")
public class ConfigSystem extends BasicTable {
    @Enumerated(EnumType.STRING)
    @Indexed(unique = true)
    private EnumConfigSystemKey key;
    private String value;
    private String description;
}
