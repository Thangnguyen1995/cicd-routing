package vn.nextpay.routing.dao.entity.sub_document;

import lombok.Data;

@Data
public class PaymentData {
    private String numberPAN;
    private String mobileUser;

}
