package vn.nextpay.routing.dao.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;

import java.time.ZonedDateTime;

@Data
public abstract class BasicTable {
    @Id
    protected String id;
    @Version
    protected int version;
    @CreatedDate
    protected ZonedDateTime createDate;
    protected String createBy;
    @LastModifiedDate
    protected ZonedDateTime updateDate;
    protected String updateBy;
}
