package vn.nextpay.routing.dao.entity.sub_document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PaymentClass {
    private String cardType;
    private String mcc;
    private String acqBank;
    private List<String> lstMid;
    private String mposMid;
    private String mposTid;
}
