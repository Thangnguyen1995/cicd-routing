package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.entity.WeightingCache;
import vn.nextpay.routing.dao.entity.sub_document.PaymentClass;
import vn.nextpay.routing.dao.repository.WeightingCacheRepository;

import java.time.ZonedDateTime;
import java.util.List;

@Service
public class WeightingCacheService {
    @Autowired
    WeightingCacheRepository repository;

    public WeightingCache findActiveWeightingByPaymentClass(PaymentClass paymentClass) {
        List<WeightingCache> weightingCacheList = repository.findActiveWeightingByPaymentClass(paymentClass.getCardType(),
                paymentClass.getMcc(), paymentClass.getAcqBank(), paymentClass.getLstMid(),
                ZonedDateTime.now());
        if (weightingCacheList.size() == 1) {
            return weightingCacheList.get(0);
        } else if (weightingCacheList.size() == 0) {
            return null;
        } else {
            for (WeightingCache weightingCache : weightingCacheList) {
                repository.deleteById(weightingCache.getId());
            }
            return null;
        }
    }

    public WeightingCache save(WeightingCache weightingCache) {
        return repository.save(weightingCache);
    }

    public void deleteAll() {
        repository.deleteAll();
    }

    public List<WeightingCache> findAllActiveCache() {
        return repository.findAllActiveCache(ZonedDateTime.now());
    }

    public void deleteAllInactive() {
        repository.deleteAllInactiveCache(ZonedDateTime.now());
    }
}
