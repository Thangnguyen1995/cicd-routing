package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.LocalBinDeleteReq;
import vn.nextpay.routing.LocalBinUpsertReq;
import vn.nextpay.routing.dao.entity.LocalBin;
import vn.nextpay.routing.dao.repository.LocalBinRepository;

import java.util.List;
import java.util.Optional;

@Service
public class LocalBinService {
    @Autowired
    private LocalBinRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public Optional<LocalBin> findById(String id) {
        return repository.findById(id);
    }

    public LocalBin save(LocalBin localBin) {
        return repository.save(localBin);
    }

    public LocalBin upsert (LocalBinUpsertReq request) {
       return mongoTemplate.findAndModify(
                Query.query(new Criteria().andOperator(
                        Criteria.where("bin").is(request.getBin())
                )),
                Update.update("bin", request.getBin())
                        .set("bank", request.getBank())
                        .set("status", CommonStatus.valueOf(request.getStatus())),
                FindAndModifyOptions.options().upsert(true),
                LocalBin.class
        );
    }

    public void deleteSync (LocalBinDeleteReq request){
        mongoTemplate.findAndRemove(Query.query(new Criteria().andOperator(
                Criteria.where("bin").is(request.getBin())
        )), LocalBin.class);
    }

    public LocalBin findByBin(String bin) {
        return repository.findByBin(bin);
    }

    public List<LocalBin> findAll() {
        return repository.findAll();
    }

    public List<LocalBin> findAllActive() {
        return repository.findAllByStatus(CommonStatus.ACTIVE);
    }


}
