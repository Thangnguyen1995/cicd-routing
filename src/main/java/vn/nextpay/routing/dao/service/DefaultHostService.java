package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.entity.DefaultHost;
import vn.nextpay.routing.dao.repository.DefaultHostRepository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class DefaultHostService {

    @Autowired
    DefaultHostRepository repository;

    public DefaultHost save(DefaultHost DefaultHost) {
        return repository.save(DefaultHost);
    }

    public Optional<DefaultHost> findById(String id) {
        return repository.findById(id);
    }

    public List<DefaultHost> findAll() {
        return repository.findAll();
    }

    public List<DefaultHost> findInCurrent(ZonedDateTime timeNow) {
        return repository.findCurrentConfig(timeNow, timeNow);
    }

}
