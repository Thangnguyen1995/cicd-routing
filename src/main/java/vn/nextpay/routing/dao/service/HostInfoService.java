package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.entity.HostInfo;
import vn.nextpay.routing.dao.repository.HostInfoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class HostInfoService {
    @Autowired
    HostInfoRepository repository;
    @Autowired
    private MongoTemplate mongoTemplate;

    public Optional<HostInfo> findById(String id) {
        return repository.findById(id);
    }

    public HostInfo save(HostInfo hostInfo) {
        return repository.save(hostInfo);
    }

    public Optional<HostInfo> findByCode(String code) {
        return repository.findByCode(code);
    }

    public List<HostInfo> findAll() {
        return repository.findAll();
    }

    public void deleteByCode(String code) {
        repository.deleteByCode(code);
    }


}
