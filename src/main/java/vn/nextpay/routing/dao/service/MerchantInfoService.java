package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.MerchantInfoDeleteReq;
import vn.nextpay.routing.MerchantInfoUpsertReq;
import vn.nextpay.routing.dao.entity.MerchantInfo;
import vn.nextpay.routing.dao.repository.MerchantInfoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MerchantInfoService {
    @Autowired
    MerchantInfoRepository repository;
    @Autowired
    private MongoTemplate mongoTemplate;

    public Optional<MerchantInfo> findById(String id) {
        return repository.findById(id);
    }

    public MerchantInfo save(MerchantInfo merchantInfo) {
        return repository.save(merchantInfo);
    }

    public MerchantInfo upsert (MerchantInfoUpsertReq request) {
        return mongoTemplate.findAndModify(
                Query.query(new Criteria().andOperator(
                        Criteria.where("merchantFk").is(request.getMerchantFk())
                )),
                Update.update("merchantFk", request.getMerchantFk())
                        .set("authoriserEmail", request.getAuthoriserEmail())
                        .set("mcc", request.getMcc())
                        .set("status", CommonStatus.valueOf(request.getStatus())),
                FindAndModifyOptions.options().upsert(true),
                MerchantInfo.class
        );
    }

    public void deleteSync (MerchantInfoDeleteReq request){
        mongoTemplate.findAndRemove(Query.query(new Criteria().andOperator(
                Criteria.where("merchantFk").is(request.getMerchantFk())
        )), MerchantInfo.class);
    }

    public Optional<MerchantInfo> findByMerchantFk(String merchantFk) {
        return repository.findByMerchantFk(merchantFk);
    }

    public List<MerchantInfo> findAll() {
        return repository.findAll();
    }
}
