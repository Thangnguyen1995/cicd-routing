package vn.nextpay.routing.dao.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.entity.AuditTrail;
import vn.nextpay.routing.dao.repository.AuditTrailRepository;

@Service
@Slf4j
public class AuditTrailService {
    @Autowired
    AuditTrailRepository repository;

    public AuditTrail createAuditTrail(String userName, String dbName, String action, Object description) {
        AuditTrail auditTrail = new AuditTrail();
        auditTrail.setDbName(dbName);
        auditTrail.setAction(action);
        auditTrail.setDescription(description);
        auditTrail.setCreateBy(userName);
        repository.save(auditTrail);
        log.info(auditTrail.toString());
        return auditTrail;
    }

}
