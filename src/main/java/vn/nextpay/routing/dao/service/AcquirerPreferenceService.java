package vn.nextpay.routing.dao.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.multiacq.common.enums.BasicStatusCode;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.multiacq.common.error.BusinessLogicError;
import vn.nextpay.routing.dao.entity.AcquirerPreference;
import vn.nextpay.routing.dao.entity.WeightingParam;
import vn.nextpay.routing.dao.entity.sub_document.PaymentClass;
import vn.nextpay.routing.dao.enumeration.WeighCriterion;
import vn.nextpay.routing.dao.repository.AcquirerPreferenceRepository;
import vn.nextpay.routing.dao.repository.WeightingParamRepository;
import vn.nextpay.routing.utils.WeightingCalculationUtils;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AcquirerPreferenceService implements WeightingCalculation {
    @Autowired
    AcquirerPreferenceRepository acquirerPreferenceRepository;

    @Autowired
    WeightingParamRepository weightingParamRepository;

    public Optional<AcquirerPreference> findById(String id) {
        return acquirerPreferenceRepository.findById(id);
    }

    public AcquirerPreference save(AcquirerPreference acquirerPreference) {
        return acquirerPreferenceRepository.save(acquirerPreference);
    }

    public List<AcquirerPreference> findByAccquirer(String acquirer) {
        return acquirerPreferenceRepository.findByAcquirer(acquirer);
    }

    public List<AcquirerPreference> findAll() {
        return acquirerPreferenceRepository.findAll();
    }

    @Override
    public Double getWeighValue(PaymentClass paymentClass, String acquirer, WeighCriterion type) {
        Double weigh = null;
        Integer preference = null;
        WeightingParam weightingParam = weightingParamRepository.findByCode(type.getName());
        if (weightingParam == null) {
            log.error("weighting param: " + type.getName() + " not found");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "weighting param: " + type.getName() + " not found");
        }
        if (weightingParam.getStatus() != CommonStatus.ACTIVE) {
            log.error("weighting param: " + type.getName() + " suspended");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "weighting param: " + type.getName() + " suspended");
        }
        List<AcquirerPreference> lstAcquirerPreferences = this.findByAccquirer(acquirer);
        if (lstAcquirerPreferences == null || lstAcquirerPreferences.size() < 1) {
            log.error("can not get acquirer fee scheme!");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "can not get acquirer preferences!");
        }
        AcquirerPreference acquirerPreference = lstAcquirerPreferences.get(0);
        preference = acquirerPreference.getPreference();
        Double high = weightingParam.getHigh();
        Double low = weightingParam.getLow();
        Double sHigh = weightingParam.getScaleHigh();
        Double sLow = weightingParam.getScaleLow();
        Double weighPoint = weightingParam.getWeightPoint();
        if (WeightingCalculationUtils.weighIsErr(high, low, sHigh, sLow, weighPoint)) {
            log.error("weighting param contains null value!");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "weighting param contains null value!");
        }
        weigh = WeightingCalculationUtils.getWeighResult(preference, high, low, sHigh, sLow, weighPoint);
        return weigh;
    }
}
