package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.entity.ExcludedHost;
import vn.nextpay.routing.dao.repository.ExcludedHostRepository;

import java.time.ZonedDateTime;
import java.util.List;

@Service
public class ExcludedHostService {

    @Autowired
    ExcludedHostRepository repository;

    public ExcludedHost save(ExcludedHost excludedHost) {
        return repository.save(excludedHost);
    }

    public List<ExcludedHost> findAll() {
        return repository.findAll();
    }

    public List<ExcludedHost> findInCurrent(ZonedDateTime timeNow) {
        return repository.findCurrentConfig(timeNow, timeNow);
    }

}
