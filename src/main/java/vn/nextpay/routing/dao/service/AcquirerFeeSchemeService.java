package vn.nextpay.routing.dao.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import vn.nextpay.multiacq.common.enums.BasicStatusCode;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.multiacq.common.error.BusinessLogicError;
import vn.nextpay.routing.dao.entity.AcquirerFeeScheme;
import vn.nextpay.routing.dao.entity.WeightingParam;
import vn.nextpay.routing.dao.entity.sub_document.PaymentClass;
import vn.nextpay.routing.dao.enumeration.WeighCriterion;
import vn.nextpay.routing.dao.repository.AcquirerFeeSchemeRepository;
import vn.nextpay.routing.dao.repository.WeightingParamRepository;
import vn.nextpay.routing.utils.WeightingCalculationUtils;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AcquirerFeeSchemeService implements WeightingCalculation {

    @Autowired
    AcquirerFeeSchemeRepository acquirerFeeSchemeRepository;

    @Autowired
    WeightingParamRepository weightingParamRepository;

    private final MongoTemplate mongoTemplate;

    public AcquirerFeeSchemeService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Optional<AcquirerFeeScheme> findById(String id) {
        return acquirerFeeSchemeRepository.findById(id);
    }

    public AcquirerFeeScheme save(AcquirerFeeScheme acquirerFeeScheme) {

        return acquirerFeeSchemeRepository.save(acquirerFeeScheme);
    }

    public List<AcquirerFeeScheme> findByCardTypeLike(String cardType) {
        return acquirerFeeSchemeRepository.findByCardTypeLike(cardType);
    }

    public List<AcquirerFeeScheme> findByAcquirer(String acquirer) {
        return acquirerFeeSchemeRepository.findByAcquirer(".*" + acquirer + ".*");
    }

    public List<AcquirerFeeScheme> findByAcquirer(String acquirer, String cardType, List<String> lstMid, String mcc) {
        List<AcquirerFeeScheme> lstAcquirerFeeScheme = acquirerFeeSchemeRepository.findByAcquirer(acquirer, cardType, lstMid, mcc);
        lstAcquirerFeeScheme.sort(Comparator.comparing(AcquirerFeeScheme::getMid, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(AcquirerFeeScheme::getMcc, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(AcquirerFeeScheme::getCardType, Comparator.nullsLast(Comparator.naturalOrder())));
        return lstAcquirerFeeScheme;
    }

    public List<AcquirerFeeScheme> findAll() {
        return acquirerFeeSchemeRepository.findAll();
    }

    @Override
    public Double getWeighValue(PaymentClass paymentClass, String acquirer, WeighCriterion type) {
        Double weigh = null;
        Double fee = null;
        String acqBank = paymentClass.getAcqBank();
        String mcc = paymentClass.getMcc();
        String cardType = paymentClass.getCardType();
        List<String> lstMid = paymentClass.getLstMid();
        WeightingParam weightingParam = weightingParamRepository.findByCode(type.getName());
        if (weightingParam == null) {
            log.error("weighting param: " + type.getName() + " not found");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "weighting param: " + type.getName() + " not found");
        }
        if (weightingParam.getStatus() != CommonStatus.ACTIVE) {
            log.error("weighting param: " + type.getName() + " suspended");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "weighting param: " + type.getName() + " suspended");
        }
        List<AcquirerFeeScheme> lstAcquirerFeeScheme = this.findByAcquirer(acquirer, cardType, lstMid, mcc);
        if (lstAcquirerFeeScheme == null || lstAcquirerFeeScheme.size() < 1) {
            log.error("can not get acquirer fee scheme!");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "can not get acquirer fee scheme!");
        }
        AcquirerFeeScheme acquirerFeeScheme = lstAcquirerFeeScheme.get(0);
        if (acqBank.equalsIgnoreCase(acquirer)) {
            fee = acquirerFeeScheme.getFeeOnus();
        } else {
            fee = acquirerFeeScheme.getFeeOffus();
        }
        fee = fee / 100;
        Double high = weightingParam.getHigh();
        Double low = weightingParam.getLow();
        Double sHigh = weightingParam.getScaleHigh();
        Double sLow = weightingParam.getScaleLow();
        Double weighPoint = weightingParam.getWeightPoint();
        if (WeightingCalculationUtils.weighIsErr(high, low, sHigh, sLow, weighPoint)) {
            log.error("weighting param contains null value!");
            throw new BusinessLogicError(BasicStatusCode.HOST_INFO_NOT_FOUND, "weighting param contains null value!");
        }
        weigh = WeightingCalculationUtils.getWeighResult(fee, high, low, sHigh, sLow, weighPoint);
        return weigh;
    }

}
