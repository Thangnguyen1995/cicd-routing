package vn.nextpay.routing.dao.service;

import vn.nextpay.routing.dao.entity.sub_document.PaymentClass;
import vn.nextpay.routing.dao.enumeration.WeighCriterion;

public interface WeightingCalculation {
    Double getWeighValue(PaymentClass paymentClass, String acquirer, WeighCriterion type);
}
