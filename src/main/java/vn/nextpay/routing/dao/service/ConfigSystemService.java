package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.entity.ConfigSystem;
import vn.nextpay.routing.dao.enumeration.EnumConfigSystemKey;
import vn.nextpay.routing.dao.repository.ConfigSystemRepository;

@Service
public class ConfigSystemService {
    @Autowired
    ConfigSystemRepository repository;

    public void save(ConfigSystem configSystem) {
        repository.save(configSystem);
    }

    public ConfigSystem findByKey(EnumConfigSystemKey code) {
        return repository.findByKey(code);
    }


}
