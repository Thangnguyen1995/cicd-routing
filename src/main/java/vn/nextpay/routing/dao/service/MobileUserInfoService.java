package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.MobileUserInfoDeleteReq;
import vn.nextpay.routing.MobileUserInfoUpsertReq;
import vn.nextpay.routing.dao.entity.MobileUserInfo;
import vn.nextpay.routing.dao.repository.MobileUserInfoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MobileUserInfoService {
    @Autowired
    MobileUserInfoRepository mobileUserInfoRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    public Optional<MobileUserInfo> findById(String id) {
        return mobileUserInfoRepository.findById(id);
    }

    public List<MobileUserInfo> findAll() {
        return mobileUserInfoRepository.findAll();
    }

    public MobileUserInfo save(MobileUserInfo mobileUserInfo) {
        return mobileUserInfoRepository.save(mobileUserInfo);
    }

    public MobileUserInfo upsert (MobileUserInfoUpsertReq request) {
        return mongoTemplate.findAndModify(
                Query.query(new Criteria().andOperator(
                        Criteria.where("mobileUserId").is(request.getMobileUserId()),
                        Criteria.where("tid").is(request.getTid())
                )),
                Update.update("mobileUserId", request.getMobileUserId())
                        .set("tid", request.getTid())
                        .set("mid", request.getMid())
                        .set("acquirer", request.getAcquirer())
                        .set("merchantFk", request.getMerchantFk())
                        .set("mposMid", request.getMposMid())
                        .set("mposTid", request.getMposMid())
                        .set("status", CommonStatus.valueOf(request.getStatus())),
                FindAndModifyOptions.options().upsert(true),
                MobileUserInfo.class
        );
    }

    public void deleteSync (MobileUserInfoDeleteReq request){
        mongoTemplate.findAndRemove(Query.query(new Criteria().andOperator(
                Criteria.where("mobileUserId").is(request.getMobileUserId()),
                Criteria.where("tid").is(request.getTid())
        )), MobileUserInfo.class);
    }
    public List<MobileUserInfo> findByMobileUserId(String muid) {
        return mobileUserInfoRepository.findByMobileUserId(muid);
    }

    public List<MobileUserInfo> findByAcquirer(String acquirer) {
        return mobileUserInfoRepository.findByAcquirer(acquirer);
    }
}
