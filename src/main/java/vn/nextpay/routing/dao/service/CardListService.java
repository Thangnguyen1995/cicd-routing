package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.CardList;
import vn.nextpay.routing.dao.repository.CardListRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CardListService {
    @Autowired
    CardListRepository repository;
    @Autowired
    private MongoTemplate mongoTemplate;

    public Optional<CardList> findById(String id) {
        return repository.findById(id);
    }

    public CardList save(CardList cardList) {
        return repository.save(cardList);
    }

    public Optional<CardList> findByName(String code) {
        return repository.findByName(code);
    }

    public List<CardList> findAll() {
        return repository.findAll();
    }

    public List<CardList> findAllActive() {
        return repository.findAllByStatus(CommonStatus.ACTIVE);
    }

    public String getCardType(String number) {
        if (!(number == null || number.length() < 10)) {
            String first10Digit = number.substring(0, 10);
            List<CardList> cardLists = this.findAllActive();
            if (cardLists == null || cardLists.size() < 1) {
                return "MPQR";
            }
            for (CardList cardList : cardLists) {
                String[] arrRange = cardList.getBinRange().split("\\|");
                for (String range : arrRange) {
                    if (checkBinInRange(first10Digit, range)) {//NOSONAR
                        return cardList.getName();
                    }
                }
            }
        }
        return "MPQR";
    }

    private boolean checkBinInRange(String bin, String range) {
        String[] arrRange = range.split("-");
        if (arrRange.length < 2) {
            return false;
        }
        String startRange = arrRange[0];
        String endRange = arrRange[1];
        return bin.compareTo(startRange) >= 0 && bin.compareTo(endRange) <= 0;
    }

}
