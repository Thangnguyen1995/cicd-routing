package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.routing.dao.entity.DefaultPair;
import vn.nextpay.routing.dao.entity.sub_document.PaymentClass;
import vn.nextpay.routing.dao.repository.DefaultPairRepository;

import java.util.Optional;

@Service
public class DefaultPairService {

    @Autowired
    DefaultPairRepository repository;


    public DefaultPair save(DefaultPair DefaultPair) {
        return repository.save(DefaultPair);
    }

    public Optional<DefaultPair> findByPaymentClass(PaymentClass paymentClass) {
        return repository.findByPaymentClass(paymentClass.getCardType(),
                paymentClass.getMcc(), paymentClass.getAcqBank(), paymentClass.getLstMid(), paymentClass.getMposMid(), paymentClass.getMposTid());
    }


}
