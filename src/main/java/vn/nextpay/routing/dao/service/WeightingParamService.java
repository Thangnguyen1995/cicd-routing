package vn.nextpay.routing.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.WeightingParam;
import vn.nextpay.routing.dao.enumeration.WeighCriterion;
import vn.nextpay.routing.dao.repository.WeightingParamRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WeightingParamService {
    @Autowired
    WeightingParamRepository weightingParamRepository;

    @Autowired
    private AcquirerFeeSchemeService acquirerFeeSchemeService;

    @Autowired
    private AcquirerPreferenceService acquirerPreferenceService;

    public Optional<WeightingParam> findById(String id) {
        return weightingParamRepository.findById(id);
    }

    public WeightingParam save(WeightingParam weightingParam) {
        return weightingParamRepository.save(weightingParam);
    }

    public List<WeightingParam> findAll() {
        return weightingParamRepository.findAll();
    }

    public WeightingParam findByCode(String code) {
        return weightingParamRepository.findByCode(code);
    }

    public WeightingParam deleteByCode(String code) {
        return weightingParamRepository.deleteByCode(code);
    }

    public List<WeighCriterion> weighCriterionList() {
        List<WeighCriterion> lstWeighCriterion = null;
        List<WeightingParam> weightingParamList = weightingParamRepository.findByStatus(CommonStatus.ACTIVE);
        if (weightingParamList != null && weightingParamList.size() > 0) {
            lstWeighCriterion = weightingParamList.stream().map(item -> WeighCriterion.getWeighCriterionByName(item.getCode()))
                    .collect(Collectors.toList());
        }
        return lstWeighCriterion;
    }

    public WeightingCalculation getWeighCriterionService(WeighCriterion type) {
        switch (type) {
            case ACQUIRER_FEE:
                return acquirerFeeSchemeService;
            case ACQUIRER_PREFERENCE:
                return acquirerPreferenceService;
//            case LVOL_DAILY_PERF:
//                return lvolDailyPerfCaculationService;
//            case UVOL_DAILY_PERF:
//                return uvolDailyPerfCaculationService;
            default:
                return null;
        }
    }

}
