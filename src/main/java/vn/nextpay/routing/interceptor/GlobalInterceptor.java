package vn.nextpay.routing.interceptor;

import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import org.lognet.springboot.grpc.GRpcGlobalInterceptor;
import vn.nextpay.multiacq.common.error.BusinessLogicError;

@GRpcGlobalInterceptor
public class GlobalInterceptor implements ServerInterceptor {

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
        ServerCall.Listener<ReqT> listener = serverCallHandler.startCall(serverCall, metadata);
        return new GlobalInterceptorServerCallListener<>(listener, serverCall, metadata);
    }

    private static class GlobalInterceptorServerCallListener<ReqT, RespT>
            extends ForwardingServerCallListener.SimpleForwardingServerCallListener<ReqT> {
        private final ServerCall<ReqT, RespT> serverCall;
        private final Metadata metadata;

        GlobalInterceptorServerCallListener(ServerCall.Listener<ReqT> listener, ServerCall<ReqT, RespT> serverCall,
                                            Metadata metadata) {
            super(listener);
            this.serverCall = serverCall;
            this.metadata = metadata;
        }

        @Override
        public void onHalfClose() {
            try {
                super.onHalfClose();
            } catch (Exception ex) {
                handleException(ex, serverCall, metadata);
                throw ex;
            }
        }

        @Override
        public void onReady() {
            try {
                super.onReady();
            } catch (Exception ex) {
                handleException(ex, serverCall, metadata);
                throw ex;
            }
        }

        private void handleException(Exception exception, ServerCall<ReqT, RespT> serverCall, Metadata metadata) {
            if (exception instanceof BusinessLogicError) {
                BusinessLogicError error = (BusinessLogicError) exception;
                metadata.put(Metadata.Key.of("errorCode", Metadata.ASCII_STRING_MARSHALLER), error.getStatus().getErrorCode());
                metadata.put(Metadata.Key.of("message", Metadata.ASCII_STRING_MARSHALLER), error.getMessage());
                StatusRuntimeException statusRuntimeException = new StatusRuntimeException(error.getStatus().getGRPCStatus(), metadata);
                serverCall.close(statusRuntimeException.getStatus(), metadata);
            }

            if (exception instanceof IllegalArgumentException) {
                serverCall.close(Status.INVALID_ARGUMENT.withDescription(exception.getMessage()), metadata);
            } else {
                System.out.println("else");
                serverCall.close(Status.UNKNOWN, metadata);
            }
        }
    }
}
