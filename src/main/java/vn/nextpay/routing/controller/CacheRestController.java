package vn.nextpay.routing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.nextpay.routing.service.CacheRestService;

@RestController
@RequestMapping("/cache")
public class CacheRestController {

    @Autowired
    private CacheRestService cacheRestService;

    @GetMapping("/delete-all")
    public void deleteAll() {
        cacheRestService.cacheDeleteAll();
    }


    @GetMapping("/retrieve-all")
    public void retrieveAll() {
        cacheRestService.cacheRetrieveAll();
    }


}
