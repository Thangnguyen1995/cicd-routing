package vn.nextpay.routing.controller;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import vn.nextpay.routing.MobileUserInfoDeleteReq;
import vn.nextpay.routing.MobileUserInfoDeleteRes;
import vn.nextpay.routing.MobileUserInfoServiceGrpc;
import vn.nextpay.routing.MobileUserInfoUpsertReq;
import vn.nextpay.routing.MobileUserInfoUpsertRes;
import vn.nextpay.routing.dao.entity.MobileUserInfo;
import vn.nextpay.routing.dao.service.MobileUserInfoService;

@GRpcService
@Slf4j
public class MobileUserInfoRestGRPC extends MobileUserInfoServiceGrpc.MobileUserInfoServiceImplBase {

    @Autowired
    MobileUserInfoService mobileUserInfoService;

    @Override
    public void mobileUserInfoUpsert(MobileUserInfoUpsertReq request, StreamObserver<MobileUserInfoUpsertRes> responseObserver) {
        log.info("mobileUserInfo upsert req = {}", request);
        mobileUserInfoService.upsert(request);
        responseObserver.onNext(MobileUserInfoUpsertRes.newBuilder().setMessage("SUCCESS").build());
        responseObserver.onCompleted();
    }

    @Override
    public void mobileUserInfoDelete(MobileUserInfoDeleteReq request, StreamObserver<MobileUserInfoDeleteRes> responseObserver) {
        log.info("mobileUserInfo delete req = {}", request);
        mobileUserInfoService.deleteSync(request);
        responseObserver.onNext(MobileUserInfoDeleteRes.newBuilder().setMessage("SUCCESS").build());
        responseObserver.onCompleted();
    }
}
