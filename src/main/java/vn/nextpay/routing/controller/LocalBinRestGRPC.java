package vn.nextpay.routing.controller;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import vn.nextpay.routing.AcquirerServiceGrpc;
import vn.nextpay.routing.GetAcqFromCoreWfReq;
import vn.nextpay.routing.GetAcqFromCoreWfRes;
import vn.nextpay.routing.LocalBinDeleteReq;
import vn.nextpay.routing.LocalBinDeleteRes;
import vn.nextpay.routing.LocalBinServiceGrpc;
import vn.nextpay.routing.LocalBinUpsertReq;
import vn.nextpay.routing.LocalBinUpsertRes;
import vn.nextpay.routing.SuspendAcquirerReq;
import vn.nextpay.routing.SuspendAcquirerRes;
import vn.nextpay.routing.dao.entity.LocalBin;
import vn.nextpay.routing.dao.entity.sub_document.PaymentData;
import vn.nextpay.routing.dao.service.LocalBinService;
import vn.nextpay.routing.service.AcquirerRestService;

@GRpcService
@Slf4j
public class LocalBinRestGRPC extends LocalBinServiceGrpc.LocalBinServiceImplBase {

    @Autowired
    private LocalBinService localBinService;

    @Override
    public void localBinUpsert(LocalBinUpsertReq request, StreamObserver<LocalBinUpsertRes> responseObserver) {
        log.info("localBin upsert req = {}", request);
        localBinService.upsert(request);
        responseObserver.onNext(LocalBinUpsertRes.newBuilder().setMessage("SUCCESS").build());
        responseObserver.onCompleted();

    }

    @Override
    public void localBinDelete(LocalBinDeleteReq request, StreamObserver<LocalBinDeleteRes> responseObserver) {
        log.info("localBin delete req = {}", request);
        localBinService.deleteSync(request);
        responseObserver.onNext(LocalBinDeleteRes.newBuilder().setMessage("SUCCESS").build());
        responseObserver.onCompleted();
    }
}
