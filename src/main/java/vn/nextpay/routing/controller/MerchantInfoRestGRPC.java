package vn.nextpay.routing.controller;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import vn.nextpay.routing.MerchantInfoDeleteReq;
import vn.nextpay.routing.MerchantInfoDeleteRes;
import vn.nextpay.routing.MerchantInfoServiceGrpc;
import vn.nextpay.routing.MerchantInfoUpsertReq;
import vn.nextpay.routing.MerchantInfoUpsertRes;
import vn.nextpay.routing.dao.service.MerchantInfoService;

@GRpcService
@Slf4j
public class MerchantInfoRestGRPC extends MerchantInfoServiceGrpc.MerchantInfoServiceImplBase {

    @Autowired
    private MerchantInfoService merchantInfoService;

    @Override
    public void merchantInfoUpsert(MerchantInfoUpsertReq request, StreamObserver<MerchantInfoUpsertRes> responseObserver) {
        log.info("MerchantInfo upsert req = {}", request);
        merchantInfoService.upsert(request);
        responseObserver.onNext(MerchantInfoUpsertRes.newBuilder().setMessage("SUCCESS").build());
        responseObserver.onCompleted();
    }

    @Override
    public void merchantInfoDelete(MerchantInfoDeleteReq request, StreamObserver<MerchantInfoDeleteRes> responseObserver) {
        log.info("MerchantInfo delete req = {}", request);
        merchantInfoService.deleteSync(request);
        responseObserver.onNext(MerchantInfoDeleteRes.newBuilder().setMessage("SUCCESS").build());
        responseObserver.onCompleted();
    }
}
