package vn.nextpay.routing.controller;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import vn.nextpay.routing.AcquirerServiceGrpc;
import vn.nextpay.routing.GetAcqFromCoreWfReq;
import vn.nextpay.routing.GetAcqFromCoreWfRes;
import vn.nextpay.routing.SuspendAcquirerReq;
import vn.nextpay.routing.SuspendAcquirerRes;
import vn.nextpay.routing.dao.entity.sub_document.PaymentData;
import vn.nextpay.routing.service.AcquirerRestService;

@GRpcService
@Slf4j
public class AcquirerRestGRPC extends AcquirerServiceGrpc.AcquirerServiceImplBase {

    @Autowired
    private AcquirerRestService acquirerRestService;


    @Override
    public void getAcqFromCoreWf(GetAcqFromCoreWfReq request, StreamObserver<GetAcqFromCoreWfRes> responseObserver) {
        log.info("req = {}", request);
        PaymentData paymentData = new PaymentData();
        paymentData.setNumberPAN(request.getNumberPAN());
        paymentData.setMobileUser(request.getMobileUser());
        GetAcqFromCoreWfRes getAcqFromCoreWfRes = acquirerRestService.getAcqForPaymentData(paymentData);
        log.info("res = {}", getAcqFromCoreWfRes);
        responseObserver.onNext(getAcqFromCoreWfRes);
        responseObserver.onCompleted();

    }

    @Override
    public void suspendAcquirer(SuspendAcquirerReq request, StreamObserver<SuspendAcquirerRes> responseObserver) {
        SuspendAcquirerRes suspendAcquirerRes = acquirerRestService.suspendAnAcq(request);
        responseObserver.onNext(suspendAcquirerRes);
        responseObserver.onCompleted();
    }

    @Override
    public void removeAnAcq(SuspendAcquirerReq request, StreamObserver<SuspendAcquirerRes> responseObserver) {
        SuspendAcquirerRes suspendAcquirerRes = acquirerRestService.removeAnAcq(request);
        responseObserver.onNext(suspendAcquirerRes);
        responseObserver.onCompleted();
    }
}
