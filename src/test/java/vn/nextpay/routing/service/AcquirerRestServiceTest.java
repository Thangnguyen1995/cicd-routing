package vn.nextpay.routing.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.routing.dao.entity.sub_document.PaymentData;

@RunWith(SpringRunner.class)
@SpringBootTest
@GRpcService
public class AcquirerRestServiceTest {
    @Autowired
    CacheRestService cacheRestService;

    @Test
    public void getAcqForPaymentDataStep1Return() {
        PaymentData paymentData = new PaymentData();
        paymentData.setMobileUser("newbv10421");
        paymentData.setNumberPAN("123456");
    }

    @Test
    public void getAcqForPaymentDataStep2aReturn() {
        PaymentData paymentData = new PaymentData();
        paymentData.setMobileUser("newbv10065");
        paymentData.setNumberPAN("123456");
    }

    @Test
    public void getAcqForPaymentDataStep2bReturn() {
        PaymentData paymentData = new PaymentData();
        paymentData.setMobileUser("newbv10064");
        paymentData.setNumberPAN("123456");
    }

    @Test
    public void getAcqForPaymentDataStep3Return() {
        PaymentData paymentData = new PaymentData();
        paymentData.setMobileUser("");
        paymentData.setNumberPAN("123456");
    }
}