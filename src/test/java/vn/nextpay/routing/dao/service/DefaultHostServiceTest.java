package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.DefaultHost;
import vn.nextpay.routing.service.BaseService;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@GRpcService
public class DefaultHostServiceTest extends BaseService {
    @Test
    public void save() {
        DefaultHost defaultHost = new DefaultHost();
        defaultHost.setCode("PVCOMBANK");
        defaultHost.setReason("Theo yeu cau");
        defaultHost.setTimeFrom(ZonedDateTime.parse("2020-04-03T00:00:00+07:00[Asia/Bangkok]"));
        defaultHost.setTimeEnd(ZonedDateTime.parse("2020-05-30T00:00:00+07:00[Asia/Bangkok]"));
        defaultHost.setCreateBy("hieunm");
        defaultHost.setStatus(CommonStatus.ACTIVE);
        defaultHost = defaultHostService.save(defaultHost);
        auditTrailService.createAuditTrail("hieunm", "DefaultHost", "CREATE", defaultHost);
    }

    @Test
    public void testFindCurrent() {
        List<DefaultHost> defaultHostList = defaultHostService.findInCurrent(ZonedDateTime.now());
        for (DefaultHost defaultHost : defaultHostList) {
            System.out.println("defaultHost==> " + defaultHost);
        }
    }

    @Test
    public void udpate() {
        Optional<DefaultHost> defaultHost = defaultHostService.findById("5ed866645e6b4b6f6310943b");
        if (defaultHost.isPresent()) {
            System.out.println("default Host ==> " + defaultHost.get());
            DefaultHost defaultHost1 = defaultHost.get();
            defaultHost1.setReason("XXXXXXXXXXXXXXXX");
            defaultHostService.save(defaultHost1);
        }
    }
}