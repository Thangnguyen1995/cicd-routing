package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.WeightingParam;

import java.time.ZonedDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@GRpcService
public class WeightingParamTest {

    @Autowired
    private WeightingParamService weightingParamService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void testBulkSave() {
        int count = 0;
        int batch = 100;
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, WeightingParam.class);
        List<WeightingParam> lsWeightingParams = weightingParamService.findAll();
        for (WeightingParam myClass : lsWeightingParams) {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(myClass.getId());
            query.addCriteria(criteria);
            Update update = new Update();
            update.set("status", CommonStatus.ACTIVE);
            update.set("createDate", ZonedDateTime.now());
            update.set("createBy", "hieunm");
            update.set("version", 1);
            update.set("updateDate", ZonedDateTime.now());
            update.set("updateBy", "hieunm");
            bulkOps.updateOne(query, update);
            count++;
            if (count == batch) {
                bulkOps.execute();
                count = 0;
            }
        }
        if (count > 0) {
            bulkOps.execute();
        }
    }

    @Test
    public void testSave() {
        WeightingParam weightingParam1 = new WeightingParam();

        weightingParam1.setCode("Acquirer Fee");
        weightingParam1.setUnit("%");
        weightingParam1.setWeightPoint(0.5d);
        weightingParam1.setLow(0.0001d);
        weightingParam1.setHigh(0.03d);
        weightingParam1.setScaleLow(100d);
        weightingParam1.setScaleHigh(0d);
        weightingParam1.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam1);

        WeightingParam weightingParam2 = new WeightingParam();
        weightingParam2.setCode("Acquirer Preference");
        weightingParam2.setUnit("0 - 9");
        weightingParam2.setWeightPoint(0.1d);
        weightingParam2.setLow(1d);
        weightingParam2.setHigh(9d);
        weightingParam2.setScaleLow(0d);
        weightingParam2.setScaleHigh(100d);
        weightingParam2.setStatus(CommonStatus.ACTIVE);
        weightingParamService.save(weightingParam2);

        WeightingParam weightingParam3 = new WeightingParam();
        weightingParam3.setCode("UVol.Daily_perf");
        weightingParam3.setUnit("% - num");
        weightingParam3.setWeightPoint(0.05d);
        weightingParam3.setLow(0.6d);
        weightingParam3.setHigh(1d);
        weightingParam3.setScaleLow(100d);
        weightingParam3.setScaleHigh(0d);
        weightingParam3.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam3);

        WeightingParam weightingParam4 = new WeightingParam();
        weightingParam4.setCode("UVol.Weekly_perf");
        weightingParam4.setUnit("% - num");
        weightingParam4.setWeightPoint(0.05d);
        weightingParam4.setLow(0.6d);
        weightingParam4.setHigh(1d);
        weightingParam4.setScaleLow(100d);
        weightingParam4.setScaleHigh(0d);
        weightingParam4.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam4);

        WeightingParam weightingParam5 = new WeightingParam();
        weightingParam5.setCode("UVol.Monthly_perf");
        weightingParam5.setUnit("% - num");
        weightingParam5.setWeightPoint(0.05d);
        weightingParam5.setLow(0.6d);
        weightingParam5.setHigh(1d);
        weightingParam5.setScaleLow(100d);
        weightingParam5.setScaleHigh(0d);
        weightingParam5.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam5);

        WeightingParam weightingParam6 = new WeightingParam();
        weightingParam6.setCode("UVol.Campaign_perf");
        weightingParam6.setUnit("% - num");
        weightingParam6.setWeightPoint(0.05d);
        weightingParam6.setLow(0.6d);
        weightingParam6.setHigh(1d);
        weightingParam6.setScaleLow(100d);
        weightingParam6.setScaleHigh(0d);
        weightingParam6.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam6);

        WeightingParam weightingParam7 = new WeightingParam();
        weightingParam7.setCode("LVol.Daily_perf");
        weightingParam7.setUnit("% - num");
        weightingParam7.setWeightPoint(0.05d);
        weightingParam7.setLow(0.6d);
        weightingParam7.setHigh(1d);
        weightingParam7.setScaleLow(100d);
        weightingParam7.setScaleHigh(0d);
        weightingParam7.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam7);

        WeightingParam weightingParam8 = new WeightingParam();
        weightingParam8.setCode("LVol.Weekly_perf");
        weightingParam8.setUnit("% - num");
        weightingParam8.setWeightPoint(0.05d);
        weightingParam8.setLow(0.6d);
        weightingParam8.setHigh(1d);
        weightingParam8.setScaleLow(100d);
        weightingParam8.setScaleHigh(0d);
        weightingParam8.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam8);

        WeightingParam weightingParam9 = new WeightingParam();
        weightingParam9.setCode("LVol.Monthly_perf");
        weightingParam9.setUnit("% - num");
        weightingParam9.setWeightPoint(0.05d);
        weightingParam9.setLow(0.6d);
        weightingParam9.setHigh(1d);
        weightingParam9.setScaleLow(100d);
        weightingParam9.setScaleHigh(0d);
        weightingParam9.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam9);

        WeightingParam weightingParam10 = new WeightingParam();
        weightingParam10.setCode("LVol.Campaign_perf");
        weightingParam10.setUnit("% - num");
        weightingParam10.setWeightPoint(0.05d);
        weightingParam10.setLow(0.6d);
        weightingParam10.setHigh(1d);
        weightingParam10.setScaleLow(100d);
        weightingParam10.setScaleHigh(0d);
        weightingParam10.setStatus(CommonStatus.SUSPENDED);
        weightingParamService.save(weightingParam10);
        System.out.println("OKKKKK");
    }

    @Test
    public void testFind() {
        WeightingParam weightingParam = weightingParamService.findByCode("UVol.Campaign_perf");
        System.out.println(weightingParam);
    }

    @Test
    public void testDelete() {
        WeightingParam weightingParam = weightingParamService.deleteByCode("LVol.Campaign_perf");
        System.out.println("delete success ===> " + weightingParam);
    }

}