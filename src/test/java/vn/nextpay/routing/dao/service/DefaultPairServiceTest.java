package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.routing.dao.entity.DefaultPair;
import vn.nextpay.routing.dao.entity.sub_document.PaymentClass;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@RunWith(SpringRunner.class)
@SpringBootTest
@GRpcService
public class DefaultPairServiceTest {

    @Autowired
    DefaultPairService defaultPairService;

    @Test
    public void save() {
        DefaultPair defaultPair = new DefaultPair();
        defaultPair.setCardType("MASTER_LOCAL");
        defaultPair.setMcc("BAO_HIEM");
        defaultPair.setAcqBank("BIDV");
        defaultPair.setMid("00000002");
        defaultPair.setAcqResultCode("VIETINBANK");
        defaultPair.setMposMid("9999");
        defaultPair.setMposTid("9999");
        defaultPair.setCreateBy("hieunm");
        defaultPair.setUpdateBy("hieunm");
        defaultPairService.save(defaultPair);
    }

    @Test
    public void testFind() {
        PaymentClass paymentClass = new PaymentClass();
        paymentClass.setMcc("BAO_HIEM");
        paymentClass.setCardType("MASTER_LOCAL");
        paymentClass.setAcqBank("BIDV");
        paymentClass.setLstMid(Stream.of("00000002", "00000002", "140000000000299").collect(Collectors.toList()));
        Optional<DefaultPair> defaultPair = defaultPairService.findByPaymentClass(paymentClass);
        if (defaultPair.isPresent()) {
            System.out.println("data: " + defaultPair.get());
        } else {
            System.out.println("no data");
        }

    }
}