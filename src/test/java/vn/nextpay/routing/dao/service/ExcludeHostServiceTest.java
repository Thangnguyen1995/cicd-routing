package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.ExcludedHost;
import vn.nextpay.routing.service.BaseService;

import java.time.ZonedDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
//@GRpcService
public class ExcludeHostServiceTest extends BaseService {
    @Test
    public void save() {
        ExcludedHost excludedHost = new ExcludedHost();
        excludedHost.setCode("PVCOMBANK");
        excludedHost.setReason("Theo yeu cau");
        excludedHost.setTimeFrom(ZonedDateTime.parse("2020-04-03T00:00:00+07:00[Asia/Bangkok]"));
        excludedHost.setTimeEnd(ZonedDateTime.parse("2020-06-30T00:00:00+07:00[Asia/Bangkok]"));
        excludedHost.setCreateBy("hieunm");
        excludedHost.setStatus(CommonStatus.ACTIVE);
        excludedHost = excludedHostService.save(excludedHost);
        auditTrailService.createAuditTrail("hieunm", "ExcludedHost", "CREATE", excludedHost);

    }

    @Test
    public void testFind() {
        List<ExcludedHost> excludedHostList = excludedHostService.findAll();
        for (ExcludedHost excludedHost : excludedHostList) {
            System.out.println("excludeHost==> " + excludedHost);
        }
    }

    @Test
    public void testFindCurrent() {
        List<ExcludedHost> excludedHostList = excludedHostService.findInCurrent(ZonedDateTime.now());
        for (ExcludedHost excludedHost : excludedHostList) {
            System.out.println("excludeHost==> " + excludedHost);
        }
    }
}