package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.routing.dao.entity.ConfigSystem;
import vn.nextpay.routing.dao.enumeration.EnumConfigSystemKey;

@RunWith(SpringRunner.class)
@SpringBootTest
@GRpcService
public class ConfigSystemServiceTest {

    @Autowired
    private ConfigSystemService configSystemService;

    @Test
    public void save() {
        ConfigSystem configSystem = new ConfigSystem();
        configSystem.setKey(EnumConfigSystemKey.EXPIRE_DATE_CACHE);
        configSystem.setValue("10");
        configSystem.setDescription("cau hinh expire 10 day cho cache");
        configSystemService.save(configSystem);
    }
}