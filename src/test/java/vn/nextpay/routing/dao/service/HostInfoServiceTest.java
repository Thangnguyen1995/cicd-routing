package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import vn.nextpay.routing.dao.entity.HostInfo;
import vn.nextpay.routing.service.BaseService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HostInfoServiceTest extends BaseService {
    @Test
    public void testSave() {
        String[] hosts = {"SACOMBANK", "BIDV", "VIETINBANK", "PVCOMBANK"};
        for (String item : hosts) {
            HostInfo hostInfo = new HostInfo();
            hostInfo.setCode(item);
            hostInfo.setCountry("VN");
            hostInfo.setPaymentBaseUrl("https://card.xxxx.vn/mpos/bankportal");
            HostInfo obj = hostInfoService.save(hostInfo);
            auditTrailService.createAuditTrail("hieunm", "HostInfo", "CREATE", obj);
            System.out.println(obj);
        }
    }

    @Test
//    @Transactional
    public void testTransaction() {
        HostInfo hostInfo = new HostInfo();
        hostInfo.setCode("QUANG ANH 1");
        hostInfo.setCountry("VN");
        hostInfo.setPaymentBaseUrl("https://card.xxxx.vn/mpos/bankportal");
         hostInfoService.save(hostInfo);
        int a = 10/0;
        HostInfo hostInfo2 = new HostInfo();
        hostInfo2.setCode("QUANG ANH 2");
        hostInfo2.setCountry("VN");
        hostInfo2.setPaymentBaseUrl("https://card.xxxx.vn/mpos/bankportal");
         hostInfoService.save(hostInfo2);
    }


}