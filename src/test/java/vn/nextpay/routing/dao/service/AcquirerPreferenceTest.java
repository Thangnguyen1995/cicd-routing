package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.routing.dao.entity.AcquirerPreference;

import java.time.ZonedDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AcquirerPreferenceTest {

    @Autowired
    private AcquirerPreferenceService acquirerPreferenceService;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void testBatchUpdate() {
        int count = 0;
        int batch = 100;
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, AcquirerPreference.class);
        List<AcquirerPreference> lsAcquirerPreferences = acquirerPreferenceService.findAll();
        for (AcquirerPreference myClass : lsAcquirerPreferences) {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(myClass.getId());
            query.addCriteria(criteria);
            Update update = new Update();
            update.set("createDate", ZonedDateTime.now());
            update.set("createBy", "hieunm");
            update.set("version", 1);
            update.set("updateDate", ZonedDateTime.now());
            update.set("updateBy", "hieunm");
            bulkOps.updateOne(query, update);
            count++;
            if (count == batch) {
                bulkOps.execute();
                count = 0;
            }
        }
        if (count > 0) {
            bulkOps.execute();
        }
    }

    @Test
    public void testSave() {
        AcquirerPreference acquirerPreference = new AcquirerPreference();
        acquirerPreference.setAcquirer("SACOMBANK");
        acquirerPreference.setPreference(5);
        acquirerPreference.setReason("Dịch vụ tốt");
        acquirerPreferenceService.save(acquirerPreference);
        System.out.println("OKKKKK");
    }

    @Test
    public void testFind() {
        List<AcquirerPreference> lstAcquirerPreference = acquirerPreferenceService.findByAccquirer("SACOMBANK");
        for (AcquirerPreference acquirerPreference : lstAcquirerPreference) {
            System.out.println("data: " + acquirerPreference);
        }

    }
}