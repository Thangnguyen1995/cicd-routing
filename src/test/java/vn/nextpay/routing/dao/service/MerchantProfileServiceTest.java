package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.MerchantInfo;
import vn.nextpay.routing.service.BaseService;

import java.time.ZonedDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MerchantProfileServiceTest extends BaseService {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void testSave() {
        int count = 0;
        int batch = 100;
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, MerchantInfo.class);
        List<MerchantInfo> lsMerchantInfos = merchantInfoService.findAll();
        for (MerchantInfo myClass : lsMerchantInfos) {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(myClass.getId());
            query.addCriteria(criteria);
            Update update = new Update();
            update.set("status", CommonStatus.ACTIVE);
            update.set("createDate", ZonedDateTime.now());
            update.set("createBy", "hieunm");
            update.set("version", 1);
            update.set("updateDate", ZonedDateTime.now());
            update.set("updateBy", "hieunm");
            bulkOps.updateOne(query, update);
            count++;
            if (count == batch) {
                bulkOps.execute();
                count = 0;
            }
        }
        if (count > 0) {
            bulkOps.execute();
        }
    }

}