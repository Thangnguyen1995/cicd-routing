package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.multiacq.common.enums.CommonStatus;
import vn.nextpay.routing.dao.entity.AcquirerFeeScheme;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
@GRpcService
public class AcquirerFeeSchemeTest {

    @Autowired
    private AcquirerFeeSchemeService acquirerFeeSchemeService;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void testBatchUpdate() {
        int count = 0;
        int batch = 100;
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, AcquirerFeeScheme.class);
        List<AcquirerFeeScheme> lsAcquirerFeeSchemes = acquirerFeeSchemeService.findAll();
        for (AcquirerFeeScheme myClass : lsAcquirerFeeSchemes) {
            Query query = new Query();
            Criteria criteria = Criteria.where("_id").is(myClass.getId());
            query.addCriteria(criteria);
            Update update = new Update();
            update.set("createDate", ZonedDateTime.now());
            update.set("createBy", "hieunm");
            update.set("version", 1);
            update.set("updateDate", ZonedDateTime.now());
            update.set("updateBy", "hieunm");
            bulkOps.updateOne(query, update);
            count++;
            if (count == batch) {
                bulkOps.execute();
                count = 0;
            }
        }
        if (count > 0) {
            bulkOps.execute();
        }
    }

    @Test
    public void testSave() {
        AcquirerFeeScheme acquirerFeeScheme = new AcquirerFeeScheme();
        acquirerFeeScheme.setAcquirer("SACOMBANK");
        acquirerFeeScheme.setCardType("MASTER");
        acquirerFeeScheme.setFeeOnus(0d);
        acquirerFeeScheme.setFeeOffus(2.35d);
        acquirerFeeScheme.setMid("60194172");
        acquirerFeeScheme.setStatus(CommonStatus.ACTIVE);
        acquirerFeeSchemeService.save(acquirerFeeScheme);
        System.out.println("OKKKKK");
    }

    @Test
    public void testFind() {
        List<AcquirerFeeScheme> lstAcquirerFeeScheme = acquirerFeeSchemeService.findByCardTypeLike("MASTER");
        for (AcquirerFeeScheme acquirerFeeScheme : lstAcquirerFeeScheme) {
            System.out.println("data: " + acquirerFeeScheme);
        }

    }

    @Test
    public void testFind2() {
        List<String> listMid = Stream.of("140000000000149", "140000000000309", "140000000000299").collect(Collectors.toList());
        List<AcquirerFeeScheme> lstAcquirerFeeScheme = acquirerFeeSchemeService.findByAcquirer("VIETINBANK", "VISA_LOCAL", listMid, null);
        for (AcquirerFeeScheme acquirerFeeScheme : lstAcquirerFeeScheme) {
            System.out.println("data: " + acquirerFeeScheme);
        }

    }
}