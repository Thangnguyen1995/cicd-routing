package vn.nextpay.routing.dao.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import vn.nextpay.routing.dao.entity.WeightingCache;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
@GRpcService
public class WeightingCacheServiceTest {

    @Autowired
    WeightingCacheService weightingCacheService;

    @Test
    public void save() {
        List<String> listMid = Stream.of("00000002", "00000000", "00000001").sorted().collect(Collectors.toList());
        WeightingCache weightingCache = new WeightingCache();
        weightingCache.setInCardType("MPQR");
        weightingCache.setInMcc("BAO_HIEM");
        weightingCache.setInAcqBank("OTHER");
        weightingCache.setInMid(listMid);
        weightingCache.setOutAcqCodeResult("SACOMBANK");
        weightingCache.setCreatedDate(ZonedDateTime.now());
        weightingCache.setExpiredDate(ZonedDateTime.now().plusYears(1));
        weightingCacheService.save(weightingCache);
    }

    @Test
    public void testFindActive() {
        List<WeightingCache> weightingCacheList = weightingCacheService.findAllActiveCache();
        for (WeightingCache weightingCache : weightingCacheList) {
            System.out.println("thong tin cache: " + weightingCache);
        }
    }

    @Test
    public void deleteInactive() {
        weightingCacheService.deleteAllInactive();
    }
}